import React from "react";
//import { Link } from "react-router-dom";
import { Field, reduxForm } from "redux-form";
import validator from "validator";
import { connect } from "react-redux";
import { API, graphqlOperation } from "aws-amplify";
import { load } from "../../../actions";
import { reloadAction } from "../../../actions";

const mutationUpdateProfile = `mutation abcd($input : updateAdminProfile){
  updateProfile(input : $input){
    email
  }
}`;

function checkForSpecialCharacter(v) {
  var format = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
  if (format.test(v)) {
    return true;
  }
  return true;
}

const validate = (formValues) => {
  let error = {};
  if (!formValues.name) {
    error.name = "* Please provide a full Name";
  }
  if (formValues.name && !checkForSpecialCharacter(formValues.name)) {
    error.name = "* Name cannot have special character";
  }
  if (!formValues.alternate_email) {
    error.alternate_email = "* Please provide an alternate email";
  }
  if (
    formValues.alternate_email &&
    !validator.isEmail(formValues.alternate_email)
  ) {
    error.alternate_email = "* Please provide a valid alternate email";
  }
  if (!formValues.college_name) {
    error.college_name = "* Please provide a College Name";
  }
  if (!formValues.phone_number) {
    //console.log("formValues.phone_number", formValues.phone_number);
    error.phone_number = "Please provide a Phone Name";
  }
  if (formValues.phone_number && formValues.phone_number.length !== 10) {
    //console.log("formValues.phone_number", formValues.phone_number);
    error.phone_number = "* Please provide a 10 digit Phone Name";
  }
  if (!formValues.pincode) {
    //console.log("formValues.pincode", formValues.pincode);
    error.pincode = "* Please provide a Pincode";
  }
  if (formValues.pincode && formValues.pincode.toString().length !== 6) {
    // console.log(
    //   "formValues.pincode",
    //   formValues.pincode,
    //   typeof formValues.pincode,
    //   formValues.pincode.length
    // );
    error.pincode = "* Please provide a 6 digit Pincode";
  }
  if (!formValues.state) {
    error.state = "* Please provide a State name";
  }
  if (!formValues.country) {
    error.country = "* Please provide a Country name";
  }
  return error;
};
/*
IsTrial: 1
​TrialEndDate: "0000-00-00"
​UniCode: "TR002"
​Uni_Code: "TR002"
​Uni_Country: "weg"
​Uni_Logo: "Null"
​Uni_Name: "RKDF"
​Uni_PostalCode: "123456"
​UserActive: "True"
​UserCode: "USR002"
​UserDOB: null
​UserDesignation: null
​UserEmail: "vishalg485@gmail.com"
​UserName: "Vishal"
​UserPhone: "+1234567891"
​UserPhoto: null
​UserRegistrationNo: null
​UserRole: "Admin"
​UserRollNo: null
​created_at: null
​idUniversityInfo: 125
​idUserInfo: 18
​updated_at: null
*/
class Profile extends React.Component {
  render() {
    console.log("this.props.myprofilemyprofile", this.props.myProfile);
    console.log(
      "​Uni_Country",
      this.props.myProfile.Uni_Country,
      "",
      this.props.myProfile["​Uni_Name"]
    );
    let phoneNumber = this.props.myProfile.UserPhone;
    let country = this.props.myProfile.Uni_Country;
    let name = this.props.myProfile.UserName;
    let role = this.props.myProfile.UserRole;
    let trial = this.props.myProfile.IsTrial;
    let college_name = this.props.myProfile["Uni_Name"];
    //{​UserPhone,​Uni_Country,​Uni_Name}=this.props.myProfile
    console.log("gggg", phoneNumber, country);
    return (
      <table className="ui selectable table">
        <thead>
          {/* <tr>
            <th>Name</th>
            <th>Status</th>
            <th className="right aligned">Notes</th>
          </tr> */}
        </thead>
        <tbody>
          <tr>
            <td>Name</td>
            <td>{name}</td>
          </tr>
          <tr>
            <td>Phone Number</td>
            <td>{phoneNumber}</td>
          </tr>
          <tr>
            <td>College name</td>
            <td>{college_name}</td>
          </tr>
          <tr>
            <td>country</td>
            <td>{country}</td>
          </tr>
          <tr>
            <td>role</td>
            <td>{role}</td>
          </tr>
          <tr>
            <td>trial</td>
            <td>{trial === 1 ? "True" : "False"}</td>
          </tr>
        </tbody>
      </table>
    );
  }
}

class EditProfile extends React.Component {
  componentDidMount() {
    this.props.load(this.props.myProfile);
    console.log("this.props.myProfile", this.props.myProfile);
  }
  renderError({ error, touched }) {
    //console.log("error, touched", error, touched);
    if (touched && error) {
      return (
        <div className="input-group">
          <div
            className="input-group-prepend"
            style={{ backgroundColor: "red" }}
          ></div>
          <p style={{ color: "red" }}>{error}</p>
        </div>
      );
    }
  }
  onSubmit = async (formValues) => {
    // console.log("formValues", formValues);
    try {
      let input2 = {
        userCode: this.props.myProfile.UserCode,
        universityCode: this.props.myProfile.UniCode,
        email: formValues.username,
        phone_number: "+" + formValues.phone_number,
        name: formValues.name,
      };

      console.log("input2", input2);
      const result = await API.graphql(
        graphqlOperation(mutationUpdateProfile, { input: input2 }) //mutationUpdateProfile, { input: input2 })
      );
      await this.props.reloadAction(this.props.UniCode);
      console.log("result", result, this.props.UniCode);
    } catch (e) {
      console.log("error", e);
    }
  };
  renderEmailInput = ({ input, placeholder, meta, type, label, disabled }) => {
    //console.log("formValues", formProps);

    return (
      <div className="form-group">
        <label>{label}</label>
        <div className="input-group">
          <div className="input-group-prepend"></div>
          <input
            className="form-control"
            type={type}
            placeholder={placeholder}
            {...input}
            disabled={disabled}
          />
        </div>
        {this.renderError(meta)}
      </div>
    );
  };

  renderPincodeInput = ({
    input,
    placeholder,
    meta,
    type,
    label,
    disabled,
  }) => {
    //console.log("formValues", formProps);

    return (
      <div className="form-group password-field">
        <label>{label}</label>
        <div className="input-group">
          <div className="input-group-prepend"></div>
          <input
            required=""
            className="form-control"
            type={type}
            placeholder={placeholder}
            {...input}
            disabled={disabled}
          />
        </div>
        {this.renderError(meta)}
      </div>
    );
  };
  renderPhoneInput = ({ input, placeholder, meta, type, label, disabled }) => {
    //console.log("formValues", formProps);

    return (
      <div className="form-group password-field">
        <label>{label}</label>
        <div className="input-group">
          <div className="input-group-prepend"></div>
          <input
            required=""
            className="form-control"
            pattern="^[1-9][0-9]{9}$"
            type={type}
            placeholder={placeholder}
            {...input}
            disabled={disabled}
          />
        </div>
        {this.renderError(meta)}
      </div>
    );
  };
  render() {
    return (
      <form onSubmit={this.props.handleSubmit(this.onSubmit)}>
        <Field
          label="Full Name"
          type="text"
          name="name"
          component={this.renderEmailInput}
          placeholder="Full Name"
          disabled={false}
        />
        <Field
          label="Email"
          type="text"
          name="username"
          component={this.renderEmailInput}
          placeholder="Email"
          disabled={true}
        />
        <Field
          label="Phone Number"
          type="text"
          name="phone_number"
          component={this.renderPhoneInput}
          placeholder="xxxxxxxxxx"
          disabled={false}
        />
        {/* <Field
          label="Address"
          type="text"
          name="address"
          component={this.renderEmailInput}
          placeholder="Address"
          disabled={false}
        />
        <Field
          label="State"
          type="text"
          name="state"
          component={this.renderEmailInput}
          placeholder="State"
          disabled={false}
        /> */}
        {/* <Field
          label="Country"
          type="text"
          name="country"
          component={this.renderEmailInput}
          placeholder="Country"
          disabled={false}
        /> */}

        <div className="row">
          <div className="col-12">
            <button
              // disabled={!this.props.valid}
              type="submit"
              className="btn btn-primary btn-block"
            >
              Save
            </button>
          </div>
        </div>
      </form>
    );
  }
}

const FormWrapped = reduxForm({
  form: "edit profile",
  validate,
  enableReinitialize: true,
})(EditProfile);

const mapStateToProps = (state) => {
  console.log("state edit profile", state.formInitialValues.data);

  if (state.formInitialValues.data) {
    return {
      initialValues: {
        name: state.formInitialValues.data.UserName || "", //"vishal",
        username: state.formInitialValues.data.UserEmail || "",
        pincode: state.formInitialValues.data.Uni_PostalCode || "",
        country: state.formInitialValues.data.Uni_Country || "",
        state: "state" || "",
        address: "address" || "",
        college_name: state.formInitialValues.data.Uni_Name || "",
        phone_number: state.formInitialValues.data.UserPhone
          ? state.formInitialValues.data.UserPhone.toString().slice(1, 11)
          : "",
      },
      UniCode: state.formInitialValues.data.UniCode,
    };
  } else {
    return {
      initialValues: {},
      UniCode: null,
    };
  }
};

const MainEditProfile = connect(mapStateToProps, { load, reloadAction })(
  FormWrapped
);

class MyProfile extends React.Component {
  state = {
    profile: "",
    editProfile: "name",
  };
  someFunct = (name) => {
    this.setState({
      profile: "",
      editProfile: "",
      [name]: "name",
    });
  };

  render() {
    return (
      <div>
        <div
          onClick={(e) => e.stopPropagation()}
          className="ui standard modal visible active"
        >
          <div className="ui secondary pointing menu">
            <button
              className={this.state.profile === "name" ? "item active" : "item"}
              onClick={() => this.someFunct("profile")}
            >
              Profile
            </button>
            <button
              className={
                this.state.editProfile === "name" ? "item active" : "item"
              }
              onClick={() => this.someFunct("editProfile")}
            >
              EditProfile
            </button>
          </div>
          <div className="ui segment">
            {this.state.profile === "name" ? (
              <Profile myProfile={this.props.myProfile} />
            ) : (
              <React.Fragment />
            )}
            {this.state.editProfile === "name" ? (
              <MainEditProfile myProfile={this.props.myProfile} />
            ) : (
              <React.Fragment />
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default MyProfile;
