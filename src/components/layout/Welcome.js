import { Switch, Route } from "react-router-dom";
import React from "react";
import { connect } from "react-redux";
//import { Redirect } from "react-router-dom";
import { Link } from "react-router-dom";
import "../css/student.css";
import { logOutAction } from "../../actions";
// import Modal from "@material-ui/core/Modal";
// import Draggable from "react-draggable";
// import Myprofile from "./myProfile";
// import { API, graphqlOperation } from "aws-amplify";

import Schedule from "./Schedule";
import Notification from "./Notification";
import Attendance from "./Attendance";
import Banners from "./Banners";
import ExamResult from "./ExamResult";
import StudentData from "./StudentData";
import TeachingStaffData from "./TeachingStaffData";
import AluminiData from "./AluminiData";
import Help from "./Help";
import StudentCorner from "./StudentCorner";

class Welcome extends React.Component {
  state = {
    schedule: "",
    attendance: "",
    examresult: "",
    studentData: "",
    teachingstaffdata: "",
    studentcorner: "",
    aluminidata: "",
    help: "",
    banners: "",
    notification: "name",
    logout: "",
  };
  someFunct = (name) => {
    this.setState({
      schedule: "",
      attendance: "",
      examresult: "",
      studentData: "",
      teachingstaffdata: "",
      studentcorner: "",
      aluminidata: "",
      help: "",
      banners: "",
      notification: "",
      logout: "",
      [name]: "name",
    });
    if (name === "logout") {
      this.props.logOutAction();
    }
  };
  render() {
    console.log("Welcome render");
    return (
      <div className="body">
        <input type="checkbox" id="nav-toggel" />
        <div className="sidebar">
          <div className="sidebar-brand">
            <h3>
              <img src="../image/campus-favicon.png" alt="" />
              <span>Campus Live</span>
            </h3>
          </div>
          <div className="sidebar-menu">
            <ul>
              <li>
                <Link
                  to="/welcome/schedule"
                  className={this.state.schedule === "name" ? "active" : ""}
                  onClick={() => this.someFunct("schedule")}
                >
                  <span>
                    <img
                      src="../image/schedule.png"
                      width="20px"
                      height="20px"
                      alt=""
                    />
                  </span>
                  <span>Schedule</span>
                </Link>
              </li>
              <li>
                <Link
                  to="/welcome/attendance"
                  className={this.state.attendance === "name" ? "active" : ""}
                  onClick={() => this.someFunct("attendance")}
                >
                  <span>
                    <img
                      src="../image/attendance.png"
                      width="20px"
                      height="20px"
                      alt=""
                    />
                  </span>
                  <span>Attendance</span>
                </Link>
              </li>
              <li>
                <Link
                  to="/welcome/examresult"
                  className={this.state.examresult === "name" ? "active" : ""}
                  onClick={() => this.someFunct("examresult")}
                >
                  <span>
                    <img
                      src="../image/exam1.png"
                      width="20px"
                      height="20px"
                      alt=""
                    />
                  </span>
                  <span>Exam Result </span>
                </Link>
              </li>
              <li>
                <Link
                  to="/welcome"
                  className={this.state.notification === "name" ? "active" : ""}
                  onClick={() => this.someFunct("notification")}
                >
                  <span>
                    <img
                      src="../image/notification.png"
                      width="20px"
                      height="20px"
                      alt=""
                    />
                  </span>
                  <span>Notification</span>
                </Link>
              </li>
              <li>
                <Link
                  to="/welcome/studentData"
                  className={this.state.studentData === "name" ? "active" : ""}
                  onClick={() => this.someFunct("studentData")}
                >
                  <span>
                    <img
                      src="../image/student.png"
                      width="20px"
                      height="20px"
                      alt=""
                    />
                  </span>
                  <span>Student Data</span>
                </Link>
              </li>
              <li>
                <Link
                  to="/welcome/teachingstaffdata"
                  className={
                    this.state.teachingstaffdata === "name" ? "active" : ""
                  }
                  onClick={() => this.someFunct("teachingstaffdata")}
                >
                  <span>
                    <img
                      src="../image/teacher.png"
                      width="25px"
                      height="25px"
                      alt=""
                    />
                  </span>
                  <span>Teaching Staff Data</span>
                </Link>
              </li>
              <li>
                <Link
                  to="/welcome/studentcorner"
                  className={
                    this.state.studentcorner === "name" ? "active" : ""
                  }
                  onClick={() => this.someFunct("studentcorner")}
                >
                  <span>
                    <img
                      src="../image/student.png"
                      width="20px"
                      height="20px"
                      alt=""
                    />
                  </span>
                  <span>Student Corner</span>
                </Link>
              </li>
              <li>
                <Link
                  to="/welcome/aluminidata"
                  className={this.state.aluminidata === "name" ? "active" : ""}
                  onClick={() => this.someFunct("aluminidata")}
                >
                  <span>
                    <img
                      src="../image/student.png"
                      alt=""
                      style={{ width: "20px", height: "20px" }}
                    />
                  </span>
                  <span>Alumni Data</span>
                </Link>
              </li>
              <li>
                <Link
                  to="/welcome/help"
                  className={this.state.help === "name" ? "active" : ""}
                  onClick={() => this.someFunct("help")}
                >
                  <span>
                    <img
                      src="../image/help.png"
                      style={{ width: "25px", height: "25px" }}
                      alt=""
                    />
                  </span>
                  <span>Help/FAQ</span>
                </Link>
              </li>
              <li>
                <Link
                  to="/welcome/banners"
                  className={this.state.banners === "name" ? "active" : ""}
                  onClick={() => this.someFunct("banners")}
                >
                  <span>
                    <img
                      src="../image/student.png"
                      width="20px"
                      height="20px"
                      alt=""
                    />
                  </span>
                  <span>Banners</span>
                </Link>
              </li>
              <li>
                <Link
                  to="/welcome/banners"
                  className={this.state.logout === "name" ? "active" : ""}
                  onClick={() => this.someFunct("logout")}
                >
                  <span>
                    <img
                      src="../image/student.png"
                      width="20px"
                      height="20px"
                      alt=""
                    />
                  </span>
                  <span>Logout</span>
                </Link>
              </li>
            </ul>
          </div>
        </div>

        <Switch>
          <Route path="/welcome" exact component={Notification} />
          <Route path="/welcome/schedule" exact component={Schedule} />
          <Route path="/welcome/attendance" exact component={Attendance} />
          <Route path="/welcome/studentData" exact component={StudentData} />
          <Route path="/welcome/banners" exact component={Banners} />
          <Route path="/welcome/examresult" exact component={ExamResult} />
          <Route
            path="/welcome/teachingstaffdata"
            exact
            component={TeachingStaffData}
          />
          <Route path="/welcome/aluminidata" exact component={AluminiData} />
          <Route path="/welcome/help" exact component={Help} />
          <Route
            path="/welcome/studentcorner"
            exact
            component={StudentCorner}
          />
        </Switch>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.isAuthenticated,
    userData: state.userData,
  };
};
export default connect(mapStateToProps, { logOutAction })(Welcome);
/*
this is the first which gets loaded when we users signs in.
this file is in development.


*/

// import React from "react";
// import { connect } from "react-redux";
// import { Redirect } from "react-router-dom";
// class Welcome extends React.Component {
//   render() {
//     console.log("Welcome render", this.props.isAuthenticated);
//     if (this.props.isAuthenticated) {
//       let data = this.props.userData;
//       let name = data["UserName"];
//       let universityName = data["Uni_Name"];
//       console.log("data Welcome", data, name, universityName);
//       return (
//         <div>
//           <b>Name</b> {name}
//           <br />
//           <b>UniversityName</b> {universityName}
//         </div>
//       );
//     } else {
//       console.log("Welcome final");
//       return <Redirect push to="/" />;
//     }
//   }
// }

// const mapStateToProps = (state) => {
//   return {
//     isAuthenticated: state.isAuthenticated,
//     userData: state.userData,
//   };
// };
// export default connect(mapStateToProps)(Welcome);
