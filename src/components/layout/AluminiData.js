import React from "react";
import Myprofile from "./myProfile";
import { connect } from "react-redux";
import Modal from "@material-ui/core/Modal";
import Draggable from "react-draggable";
class AluminiData extends React.Component {
  state = {
    myProfile: false,
  };

  myProfile = () => {
    this.setState({
      myProfile: true,
    });
  };

  render() {
    let data = this.props.userData;
    let name = data["UserName"];
    return (
      <div className="main-content bg-light">
        <Modal
          open={this.state.myProfile}
          onClose={() => this.setState({ myProfile: false })}
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
        >
          <Draggable>
            <div
              tabIndex="-1"
              style={{
                paddingLeft: 200,
                paddingTop: 200,
                width: 250,
              }}
            >
              <Myprofile myProfile={data} />
            </div>
          </Draggable>
        </Modal>
        <header>
          <h4>
            <label htmlFor="nav-toggel">
              <span>
                <i className="fa fa-bars" aria-hidden="true"></i>
              </span>
            </label>
            <span className="name">AluminiData</span>
          </h4>

          <div className="search-wrapper">
            <span>
              <i className="fa fa-search" aria-hidden="true"></i>
            </span>
            <input type="search" placeholder="Search here" />
          </div>

          <div className="user-wrapper" onClick={this.myProfile}>
            <img src="../image/avt1.jpg" width="50px" height="50px" alt="" />
            <div>
              <h6>{name}</h6>
              <small>Student</small>
            </div>
          </div>
        </header>

        <main>
          <div className="filter-wrapper"></div>

          <div className="table-content m-3">
            <p>AluminiData</p>
          </div>
        </main>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.isAuthenticated,
    userData: state.userData,
  };
};
export default connect(mapStateToProps, null)(AluminiData);

//export default AluminiData;
