import React from "react";
import { connect } from "react-redux";

class myProfile extends React.Component {
  reSendAuthCode = (emailObject) => {
    console.log("resend");
    console.log("emailObject", emailObject);
  };
  render() {
    console.log("myProfile myProfile", this.props.myProfile);
    var status = "non - active";
    var {
      UserPhone,
      UserName,
      UserEmail,
      Uni_Name,
      IsTrial,
      UniCode,
    } = this.props.myProfile;
    console.log(UserPhone, UserName, UserEmail, Uni_Name, IsTrial, UniCode);
    if (IsTrial === 1) {
      status = "active";
    }
    return (
      <div>
        <div
          onClick={(e) => e.stopPropagation()}
          className="ui standard modal visible active"
        >
          <div className="ui message">
            <div className="header">Profile Information</div>
            <ul className="list">
              <li>{UserName}</li>
              <li>{UserPhone}</li>
              <li>{UserEmail}</li>
              <li>University {Uni_Name}</li>
              <li>University Code {UniCode}</li>
              <li>Trial Status {status}</li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  console.log("state alert ", state.alertUser.msg);
  return {
    alertObject: state.alertUser,
  };
};

export default connect(mapStateToProps, null)(myProfile);
