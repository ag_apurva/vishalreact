import React from "react";
import { Redirect } from "react-router-dom";
import Myprofile from "./myProfile";
import { API, graphqlOperation } from "aws-amplify";
import { connect } from "react-redux";
import Modal from "@material-ui/core/Modal";
import Draggable from "react-draggable";
import CreateStudents from "./popups/createStudents";
import { deleteStudentMutation } from "../../mutation";

const query1 = `
query abc($id: String!){
  studentData(collegeId : $id){
    name
    email
  }
}`;

class DeleteStudent extends React.Component {
  deleteStudent = async (email) => {
    console.log("delete student", email);
    this.props.removePopUp();
    const result = await API.graphql(
      graphqlOperation(deleteStudentMutation, { input: { email } })
    );
    console.log("result", result);
  };
  render() {
    console.log("this.props", this.props);
    return (
      <div>
        <p>Delete Student</p>
        <div className="header">edit student</div>
        <div className="content">
          <p>{this.props.myProfile.email}</p>
          <p>{this.props.myProfile.name}</p>
          <button
            className="ui button"
            onClick={() => this.deleteStudent(this.props.myProfile.email)}
          >
            Delete {this.props.myProfile.name}
          </button>
        </div>
      </div>
    );
  }
}

class EditStudents extends React.Component {
  render() {
    return (
      <div>
        <p>Edit Student</p>
      </div>
    );
  }
}

class StudentsProfile extends React.Component {
  state = {
    deleteprofile: "",
    editProfileStudent: "name",
  };
  someFunct = (name) => {
    this.setState({
      deleteprofile: "",
      editProfileStudent: "",
      [name]: "name",
    });
  };
  // deleteStudent = async (email) => {
  //   console.log("delete student", email);
  //   this.props.removePopUp();
  //   const result = await API.graphql(
  //     graphqlOperation(deleteStudentMutation, { input: { email } })
  //   );
  //   console.log("result", result);
  // };
  render() {
    console.log("this.props", this.props);
    return (
      <div>
        <div className="ui standard modal visible active">
          {/* <div className="header">edit student</div>
          <div className="content">
            <p>{this.props.myProfile.email}</p>
            <p>{this.props.myProfile.name}</p>
            <button
              className="ui button"
              onClick={() => this.deleteStudent(this.props.myProfile.email)}
            >
              Delete {this.props.myProfile.name}
            </button>
          </div> */}
          <div className="ui secondary pointing menu">
            <button
              className={
                this.state.deleteprofile === "name" ? "item active" : "item"
              }
              onClick={() => this.someFunct("deleteprofile")}
            >
              DeleteStudent
            </button>
            <button
              className={
                this.state.editProfileStudent === "name"
                  ? "item active"
                  : "item"
              }
              onClick={() => this.someFunct("editProfileStudent")}
            >
              Edit Student Profile
            </button>
          </div>
          <div className="ui segment">
            {this.state.deleteprofile === "name" ? (
              <DeleteStudent
                removePopUp={this.props.removePopUp}
                myProfile={this.props.myProfile}
              />
            ) : (
              <React.Fragment />
            )}
            {this.state.editProfileStudent === "name" ? (
              <EditStudents />
            ) : (
              <React.Fragment />
            )}
          </div>
        </div>
      </div>
    );
  }
}

class StudentData extends React.Component {
  state = {
    myProfile: false,
    addStudents: false,
    posts: [],
    editStudentPopUp: false,
    studentEmail: "",
  };
  myProfile = () => {
    this.setState({
      myProfile: true,
    });
  };
  async componentDidMount() {
    console.log("component did mount APP", this.props.userData.UniCode);
    let input2 = {
      id: this.props.userData.UniCode,
    };
    try {
      const result = await API.graphql(graphqlOperation(query1, input2));
      console.log("result", result.data.studentData);
      this.setState({ posts: result.data.studentData });
    } catch (e) {
      console.log("error appsync", e);
    }
  }

  editStudentsAddPopUp = (email) => {
    console.log("edit students add popup", email);
    this.setState({
      editStudentPopUp: true,
      studentEmail: email,
    });
  };
  editStudentsRemovePopUp = (email) => {
    console.log("edit students remove popup", email);
    this.setState({
      editStudentPopUp: false,
    });
  };
  renderStudents() {
    var posts = this.state.posts;
    console.log("posts", posts);
    return posts.map((item) => {
      let { name, email } = item;
      return (
        <tr key={name}>
          <Modal
            open={
              this.state.editStudentPopUp && this.state.studentEmail === email
            }
            onClose={() => this.setState({ editStudentPopUp: false })}
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
            aria-hidden="true"
          >
            <Draggable>
              <div
                tabIndex="-1"
                style={{
                  paddingLeft: 200,
                  paddingTop: 200,
                  width: 250,
                }}
              >
                <StudentsProfile
                  myProfile={item}
                  removePopUp={this.editStudentsRemovePopUp}
                />
              </div>
            </Draggable>
          </Modal>
          <td>
            <div className="form-check">
              <input
                className="form-check-input position-static"
                type="checkbox"
                id="blankCheckbox"
                value="option1"
                aria-label="..."
              />
            </div>
          </td>
          <td>
            <span>
              <img src="../avt3.jpg" width="50px" height="50px" alt="" />
            </span>
            <span>{name}</span>
          </td>
          <td>{email}</td>
          <td>Morning</td>
          <td>First Year</td>
          <td>F</td>
          <td>
            <button onClick={() => this.editStudentsAddPopUp(email)}>
              <i className="fa fa-ellipsis-h" aria-hidden="true"></i>
            </button>
          </td>
        </tr>
      );
    });
  }
  render() {
    console.log("Welcome render", this.props.isAuthenticated);
    if (this.props.isAuthenticated) {
      let data = this.props.userData;
      let name = data["UserName"];
      let universityName = data["Uni_Name"];
      console.log("data Welcome", data, name, universityName);
      return (
        <div className="main-content bg-light">
          <header>
            <h4>
              <label htmlFor="nav-toggel">
                <span>
                  <i className="fa fa-bars" aria-hidden="true"></i>
                </span>
              </label>
              <span className="name">Student Data</span>
            </h4>

            <div className="search-wrapper">
              <span>
                <i className="fa fa-search" aria-hidden="true"></i>
              </span>
              <input type="search" placeholder="Search here" />
            </div>

            <div className="user-wrapper" onClick={this.myProfile}>
              <img src="../image/avt1.jpg" width="50px" height="50px" alt="" />
              <div>
                <h6>{name}</h6>
                <small>Student</small>
              </div>
            </div>
          </header>
          <Modal
            open={this.state.myProfile}
            onClose={() => this.setState({ myProfile: false })}
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
            aria-hidden="true"
          >
            <Draggable>
              <div
                tabIndex="-1"
                style={{
                  paddingLeft: 200,
                  paddingTop: 200,
                  width: 250,
                }}
              >
                <Myprofile myProfile={data} />
              </div>
            </Draggable>
          </Modal>
          <Modal
            open={this.state.addStudents}
            onClose={() => this.setState({ addStudents: false })}
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
            aria-hidden="true"
          >
            <Draggable>
              <div
                tabIndex="-1"
                style={{
                  paddingLeft: 200,
                  paddingTop: 200,
                  width: 250,
                }}
              >
                <CreateStudents />
              </div>
            </Draggable>
          </Modal>
          <main>
            <div className="filter-wrapper">
              <div className="filter">
                <label>Stream</label>
                <input
                  type="text"
                  className="form-control"
                  placeholder="e.g. computer"
                />
              </div>
              <div className="filter">
                <label>Batch</label>
                <input
                  type="text"
                  className="form-control"
                  placeholder="e.g. morning"
                />
              </div>
              <div className="filter">
                <label>ClassName</label>
                <input
                  type="text"
                  className="form-control"
                  placeholder="e.g. first year"
                />
              </div>
              <div className="filter">
                <label>Division</label>
                <input
                  type="text"
                  className="form-control"
                  placeholder="e.g. A"
                />
              </div>
              <div className="filter fil-btn">
                <label></label>
                <button
                  type="submit"
                  className="btn btn-primary mb-2"
                  style={{ backgroundColor: "#4cadad" }}
                >
                  GO
                </button>
              </div>
            </div>

            <div className="table-content m-3">
              <table className="table table-hover">
                <thead>
                  <tr>
                    <th scope="col">
                      <div className="form-check">
                        <input
                          className="form-check-input position-static"
                          type="checkbox"
                          id="blankCheckbox"
                          value="option1"
                          aria-label="..."
                        />
                      </div>
                    </th>
                    <th scope="col">Student Name</th>
                    <th scope="col">Stream</th>
                    <th scope="col">Batch</th>
                    <th scope="col">ClassName</th>
                    <th scope="col">Division</th>
                    <th>Edit/Delete</th>
                  </tr>
                </thead>
                <tbody>{this.renderStudents()}</tbody>
              </table>
            </div>
            <button
              className="btn btn-primary mb-2"
              onClick={() => this.setState({ addStudents: true })}
            >
              Add Student
            </button>
          </main>
        </div>
      );
    } else {
      console.log("Welcome final");
      return <Redirect push to="/" />;
    }
  }
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.isAuthenticated,
    userData: state.userData,
  };
};
export default connect(mapStateToProps, null)(StudentData);
