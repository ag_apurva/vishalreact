import React from "react";
import { ConfimCodeAction, resendAuthCode } from "../../actions";
import { connect } from "react-redux";
import AlertComponent from "./AlertComponent";
//import ReactDOM from "react-dom";
import history from "../../history";
class Confirm extends React.Component {
  state = { username: "", code: "" };
  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };
  onSubmit = (e) => {
    console.log("submitsubmitsubmitsubmit");
    e.preventDefault();
    console.log("submit confirm called", this.state.username, this.state.code);
    this.props.ConfimCodeAction({
      username: this.state.username,
      code: this.state.code,
    });
  };
  reSendAuthCode = (emailObject) => {
    console.log("resend");
    console.log("emailObject", emailObject);
    this.props.resendAuthCode({ email: this.state.username });
  };
  render() {
    return (
      <div onClick={() => history.push("/register")}>
        <div
          onClick={(e) => e.stopPropagation()}
          className="ui standard modal visible active"
        >
          <div className="header">Confirm SignUp</div>
          <div className="content">
            <form className="ui form" onSubmit={(e) => this.onSubmit(e)}>
              <div className="field">
                <label>Email</label>
                <input
                  type="email"
                  name="username"
                  placeholder="Email"
                  value={this.state.username}
                  onChange={(e) => this.onChange(e)}
                />
              </div>
              <div className="field">
                <label>ConfirmationCode</label>
                <input
                  type="text"
                  name="code"
                  placeholder="ConfirmationCode"
                  value={this.state.code}
                  onChange={(e) => this.onChange(e)}
                />
              </div>
              <button className="ui button" type="submit">
                Submit
              </button>
            </form>
          </div>
          <div className="actions">
            <button
              className="ui button"
              onClick={() =>
                this.reSendAuthCode({ email: this.state.username })
              }
            >
              ReSend Code
            </button>
          </div>
          {this.props.alertObject.isAlert ? (
            <div className="content">
              <AlertComponent />
            </div>
          ) : (
            <React.Fragment />
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  console.log("state alert ", state.alertUser.msg);
  return {
    alertObject: state.alertUser,
  };
};

export default connect(mapStateToProps, {
  ConfimCodeAction,
  resendAuthCode,
})(Confirm);

/*
This Component gets called when user register for the first time to check in the confirmtaion code.

*/

/*
    return (

    );

      <React.Fragment>
        <form className="ui form" onSubmit={(e) => this.onSubmit(e)}>
          <div className="field">
            <label>Email</label>
            <input
              type="email"
              name="username"
              placeholder="Email"
              value={this.state.username}
              onChange={(e) => this.onChange(e)}
            />
          </div>
          <div className="field">
            <label>ConfirmationCode</label>
            <input
              type="text"
              name="code"
              placeholder="ConfirmationCode"
              value={this.state.code}
              onChange={(e) => this.onChange(e)}
            />
          </div>

          <button className="ui button" type="submit">
            Submit
          </button>
        </form>
        <button
          className="ui button"
          onClick={() => this.reSendAuthCode({ email: this.state.username })}
        >
          ReSend Code
        </button>
        
      </React.Fragment>


*/
