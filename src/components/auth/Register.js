import React from "react";
import { SignUpAction } from "../../actions";
import { connect } from "react-redux";
//import history from "../../history";
import { Link } from "react-router-dom";
import AlertComponent from "./AlertComponent";
//import { removeAlert, passwordsDoNotMatch } from "../../actions";
import Confirm from "./Confirm";
import { Field, reduxForm } from "redux-form";
import Modal from "@material-ui/core/Modal";
import Draggable from "react-draggable";
//var validator = require("validator");
import validator from "validator";
function checkPassword(str) {
  var re = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
  return re.test(str);
}

// function checkForSpecialCharacter(v) {
//   var format = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;

//   if (format.test(v)) {
//     return true;
//   }

//   return false;
// }

function checkForSpecialCharacter(v) {
  var format = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;

  if (format.test(v)) {
    return true;
  }

  return true;
}

const validate = (formValues) => {
  let error = {};
  if (!formValues.name) {
    error.name = "* Please provide a full Name";
  }
  if (formValues.name && !checkForSpecialCharacter(formValues.name)) {
    error.name = "* Name cannot have special character";
  }
  if (!formValues.username) {
    error.username = "* Please provide an email";
  }
  if (formValues.username && !validator.isEmail(formValues.username)) {
    error.username = "* Please provide a valid email";
  }
  if (!formValues.alternate_email) {
    error.alternate_email = "* Please provide an alternate email";
  }
  if (
    formValues.alternate_email &&
    !validator.isEmail(formValues.alternate_email)
  ) {
    error.alternate_email = "* Please provide a valid alternate email";
  }
  if (!formValues.college_name) {
    error.college_name = "* Please provide a College Name";
  }
  if (!formValues.phone_number) {
    //console.log("formValues.phone_number", formValues.phone_number);
    error.phone_number = "Please provide a Phone Name";
  }
  if (formValues.phone_number && formValues.phone_number.length !== 10) {
    //console.log("formValues.phone_number", formValues.phone_number);
    error.phone_number = "* Please provide a 10 digit Phone Name";
  }
  if (!formValues.pincode) {
    //console.log("formValues.pincode", formValues.pincode);
    error.pincode = "* Please provide a Pincode";
  }
  if (formValues.pincode && formValues.pincode.length !== 6) {
    // console.log("formValues.pincode", formValues.pincode);
    error.pincode = "* Please provide a 6 digit Pincode";
  }
  if (!formValues.state) {
    error.state = "* Please provide a State name";
  }
  if (!formValues.country) {
    error.country = "* Please provide a Country name";
  }
  if (!checkPassword(formValues.password)) {
    error.password =
      "* Password should have minimum of 7 character with at least one uppercase, one lowercase a number and one special symbol";
  }
  if (formValues.password !== formValues.confirmpassword) {
    error.confirmpassword = "* Confirm password does not match actual password";
  }
  return error;
};

class Register extends React.Component {
  state = { openConfirm: false };
  renderError({ error, touched }) {
    //console.log("error, touched", error, touched);
    if (touched && error) {
      return (
        <div className="input-group">
          <div
            className="input-group-prepend"
            style={{ backgroundColor: "red" }}
          ></div>
          <p style={{ color: "red" }}>{error}</p>
        </div>
      );
    }
  }

  renderEmailInput = ({ input, placeholder, meta, type, label }) => {
    //console.log("formValues", formProps);

    return (
      <div className="form-group">
        <label>{label}</label>
        <div className="input-group">
          <div className="input-group-prepend"></div>
          <input
            className="form-control"
            type={type}
            placeholder={placeholder}
            {...input}
          />
        </div>
        {this.renderError(meta)}
      </div>
    );
  };

  renderPasswordInput = ({ input, placeholder, meta, type, label }) => {
    //console.log("formValues", formProps);

    return (
      <div className="form-group password-field">
        <label>{label}</label>
        <div className="input-group">
          <div className="input-group-prepend"></div>
          <input
            required=""
            className="form-control"
            type={type}
            placeholder={placeholder}
            {...input}
          />
        </div>
        {this.renderError(meta)}
      </div>
    );
  };

  renderPincodeInput = ({ input, placeholder, meta, type, label }) => {
    //console.log("formValues", formProps);

    return (
      <div className="form-group password-field">
        <label>{label}</label>
        <div className="input-group">
          <div className="input-group-prepend"></div>
          <input
            required=""
            className="form-control"
            pattern="^[1-9][0-9]{5}$"
            type={type}
            placeholder={placeholder}
            {...input}
          />
        </div>
        {this.renderError(meta)}
      </div>
    );
  };
  renderPhoneInput = ({ input, placeholder, meta, type, label }) => {
    //console.log("formValues", formProps);

    return (
      <div className="form-group password-field">
        <label>{label}</label>
        <div className="input-group">
          <div className="input-group-prepend"></div>
          <input
            required=""
            className="form-control"
            pattern="^[1-9][0-9]{9}$"
            type={type}
            placeholder={placeholder}
            {...input}
          />
        </div>
        {this.renderError(meta)}
      </div>
    );
  };

  onSubmit = (formValues) => {
    console.log("formValues", formValues);

    const {
      name,
      username,
      address,
      phone_number,
      college_name,
      password,
      confirmpassword,
      alternate_email,
      pincode,
      state,
      country,
    } = formValues;
    this.props.SignUpAction({
      name,
      username,
      address,
      phone_number: "+" + phone_number,
      college_name,
      password,
      confirmpassword,
      alternate_email,
      pincode,
      state,
      country,
    });
    this.setState({ openConfirm: true });
  };

  render() {
    console.log("Register ");
    return (
      <div className="wrapper-signup container p-0">
        <div className="signup">
          <div className="row inner-wrap-signup">
            <div className="col-lg-6 col-md-8 col mx-auto sign-con">
              <div className="signup-login">
                <div className="login-content">
                  <div className="mb-8 campus-logo">
                    <img
                      src="image/campus-favicon.png"
                      alt="imag"
                      style={{
                        width: "20%",
                        marginBottom: "20px",
                        display: "block",
                        margin: "auto",
                      }}
                    />
                  </div>
                  <Modal
                    open={this.state.openConfirm}
                    onClose={() => this.setState({ openConfirm: false })}
                    aria-labelledby="simple-modal-title"
                    aria-describedby="simple-modal-description"
                  >
                    <Draggable>
                      <div
                        style={{
                          paddingLeft: 200,
                          paddingTop: 200,
                          width: 250,
                        }}
                      >
                        <Confirm remove={this.removeCodes} />
                      </div>
                    </Draggable>
                  </Modal>
                  <h2 className="text-center">Sign Up!</h2>
                  {/* <!--Login--> */}
                  <div
                    className="login-pane fade show active"
                    role="tabpanel"
                    id="login"
                  >
                    <p className="mt-6 mb-6 pb-1 text-center">
                      Please fill in this form to create an account!
                    </p>

                    <form onSubmit={this.props.handleSubmit(this.onSubmit)}>
                      <Field
                        label="Full Name"
                        type="text"
                        name="name"
                        component={this.renderEmailInput}
                        placeholder="Full Name"
                      />
                      <Field
                        label="Email"
                        type="text"
                        name="username"
                        component={this.renderEmailInput}
                        placeholder="Email"
                      />
                      <Field
                        label="Alternate Email"
                        type="text"
                        name="alternate_email"
                        component={this.renderEmailInput}
                        placeholder="Alternate Email"
                      />
                      <Field
                        label="College"
                        type="text"
                        name="college_name"
                        component={this.renderEmailInput}
                        placeholder="College Name"
                      />
                      <Field
                        label="Phone Number"
                        type="text"
                        name="phone_number"
                        component={this.renderPhoneInput}
                        placeholder="xxxxxxxxxx"
                      />
                      <Field
                        label="Address"
                        type="text"
                        name="address"
                        component={this.renderEmailInput}
                        placeholder="Address"
                      />
                      <Field
                        label="State"
                        type="text"
                        name="state"
                        component={this.renderEmailInput}
                        placeholder="State"
                      />
                      <Field
                        label="Country"
                        type="text"
                        name="country"
                        component={this.renderEmailInput}
                        placeholder="Country"
                      />
                      <Field
                        label="Pincode"
                        type="text"
                        name="pincode"
                        component={this.renderPincodeInput}
                        placeholder="Pincode"
                      />
                      <Field
                        label="Password"
                        type="password"
                        name="password"
                        component={this.renderPasswordInput}
                        placeholder="Password"
                      />
                      <Field
                        label="Confirm Password"
                        type="password"
                        name="confirmpassword"
                        component={this.renderPasswordInput}
                        placeholder="Confirm Password"
                      />

                      <div className="row">
                        <div className="col-12">
                          <button
                            // disabled={!this.props.valid}
                            type="submit"
                            className="btn btn-primary btn-block"
                          >
                            Register
                          </button>
                        </div>
                        <div
                          className="col-12"
                          style={{
                            paddingTop: "20px",
                            paddingLeft: "20px",
                          }}
                        >
                          <Link to="/" style={{ marginBottom: "20px" }}>
                            Already Signed Up? Login Now{" "}
                          </Link>
                          <p style={{ marginTop: "10px" }}>
                            <Link to="/" className="">
                              Back To Home Page
                            </Link>
                          </p>
                        </div>
                      </div>
                      <div className="row">
                        <AlertComponent />
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const formWrapped = reduxForm({
  form: "registerUser",
  validate,
})(Register);

export default connect(null, {
  SignUpAction,
})(formWrapped);

// console.log(
//   "name",
//   name,
//   "username",
//   username,
//   "address",
//   address,
//   "phone_number",
//   phone_number,
//   "college_name",
//   college_name,
//   "password",
//   password,
//   "confirmpassword",
//   confirmpassword,
//   "alternate_email",
//   alternate_email,
//   "pincode",
//   pincode,
//   "state",
//   state,
//   "country",
//   country
// );
