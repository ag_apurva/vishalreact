import React from "react";
//import ReactDOM from "react-dom";
// import { forgotUniversityCodes } from "../../actions";
// import ReactDOM from "react-dom";
import { connect } from "react-redux";
//import AlertComponent from "./AlertComponent";
import history from "../../history";

class ForgotUniversityCodes extends React.Component {
  renderUniversityData() {
    console.log("this.props", this.props.anotherUser.apiDetails.data);
    return this.props.anotherUser.apiDetails.data.map((item) => {
      return (
        <div className="item" key={item.UniCode}>
          <div className="left aligned content">{item.Uni_Name}</div>
          <div className="right aligned content">{item.UniCode}</div>
        </div>
      );
    });
  }
  removeModal = () => {
    this.props.remove();
  };
  render() {
    return (
      <div onClick={() => history.push("/")}>
        <div
          onClick={(e) => e.stopPropagation()}
          className="ui standard modal visible active"
        >
          <div className="header">User is Registered for these University</div>
          <div className="content">
            <div className="ui divided items">
              <div className="item">
                <div className="left aligned content">Collge</div>
                <div className="right aligned content">Code</div>
              </div>
              {this.renderUniversityData()}
            </div>
          </div>
          <div className="actions">
            <button className="ui button" onClick={this.removeModal}>
              Ok
            </button>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  console.log("state another forgot university object", state.anotherUser);
  return {
    anotherUser: state.anotherUser,
  };
};

export default connect(mapStateToProps)(ForgotUniversityCodes);

/*
this is baiscally the component which displays the forgotten university codes.


*/
