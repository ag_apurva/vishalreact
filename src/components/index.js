import React from "react";
import { Router, Route } from "react-router-dom";
import Login from "./auth/Login";
import Register from "./auth/Register";
import ForgotPassword from "./auth/ForgotPassword";
import ForgotCodesForm from "./auth/ForgotCodesForm";
import ForgotUniversityCodes from "./auth/ForgotUniversityCodes";
import RegisterAnother from "./auth/Register2";
import history from "../history";
import Welcome from "./layout/Welcome";
import StudentLogin from "./auth/StudentLogin";
import Confirm from "./auth/Confirm";
import { connect } from "react-redux";
import { reloadAction, logOutAction } from "../actions";
import StudentLoginNotAllowed from "./auth/StudentLoginNotAllowed";
class App extends React.Component {
  state = { isAuthenticating: true };
  async componentDidMount() {
    console.log("component did mount of router called");
    try {
      if (!localStorage.getItem("universityCode")) {
        console.log("singout");
        this.props.logOutAction();
      } else {
        console.log("university data", localStorage.getItem("universityCode"));
        let tr = await this.props.reloadAction(
          localStorage.getItem("universityCode")
        );
        console.log("tr", tr);
      }
    } catch (e) {
      console.log(e);
    }
    this.setState({ isAuthenticating: false });
  }
  render() {
    console.log("render of router called");
    if (this.state.isAuthenticating === false) {
      console.log("deciding point");
      if (this.state.isAuthenticated === false) {
        console.log("logout");
        history.push("/");
      } else {
        return (
          <div>
            <Router history={history}>
              <Route path="/" exact component={Login} />
              <Route path="/register" exact component={Register} />
              <Route path="/welcome" component={Welcome} />
              <Route path="/student" exact component={StudentLogin} />

              <Route
                path="/registerAnotherUser"
                exact
                component={RegisterAnother}
              />
              <Route
                path="/studentLogin"
                exact
                component={StudentLoginNotAllowed}
              />
              <Route
                path="/forgotUniversitySubmit"
                exact
                component={ForgotCodesForm}
              />
              <Route
                path="/forgotUniversityCodes"
                exact
                component={ForgotUniversityCodes}
              />
              <Route path="/confirm" exact component={Confirm} />
              <Route path="/forgotPassword" exact component={ForgotPassword} />
            </Router>
          </div>
        );
      }
    } else {
      console.log("WAITING WAITING", localStorage.getItem("universityCode"));
      return (
        <div className="ui active inverted dimmer">
          <div className="ui loader"></div>
        </div>
      );
    }
  }
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.isAuthenticated,
    userData: state.userData,
  };
};

export default connect(mapStateToProps, { reloadAction, logOutAction })(App);

/*
This is main file which get renders when we first load our React app,
this file contains router information about which paths to take when certain event occurs.
<Route> tag is responsible for loading the corresponding component
defualt component being LOGIN.
Router has history object being passed to it,
which is responsible to route pages when certain eevent occurs.
*/
