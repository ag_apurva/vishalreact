import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";

const isAuthenticated = function (isAuth = false, action) {
  if (action.type === "IS_AUTHENTICATED") {
    return action.payload;
  }
  return isAuth;
};

const userData = function (user = null, action) {
  if (action.type === "LOGIN_SUCCESS") {
    return action.payload;
  }
  return user;
};

const anotherUser = function (user = null, action) {
  if (action.type === "ANOTHER_USER") {
    return action.payload;
  }
  return user;
};

const alertUser = function (alert = { isAlert: false, msg: "" }, action) {
  if (action.type === "LOGIN_FAILED" || action.type === "REGISTER_FAILED") {
    console.log("check");
    let obj = { isAlert: true, msg: action.payload };
    return obj;
  }
  if (action.type === "REMOVE_ALERT") {
    console.log("check2");
    let obj = { isAlert: false, msg: "" };
    return obj;
  }
  return alert;
};

const reducer = (state = {}, action) => {
  switch (action.type) {
    case "LOAD":
      return {
        data: action.data,
      };
    default:
      return state;
  }
};

export default combineReducers({
  isAuthenticated,
  userData,
  alertUser,
  form: formReducer,
  anotherUser,
  formInitialValues: reducer,
});

/*
this file is responsible for reloding the entire component when certain event occurs.
the dispatch from actions reaches over here, and then passed to the respective component.


*/
