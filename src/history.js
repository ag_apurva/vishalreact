import { createBrowserHistory } from "history";
export default createBrowserHistory();

//this is history object being passed to the router that helps us route between pages.
